﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CharacterHud : MonoBehaviour {
	public GameObject healthIconPrefab;

	HorizontalLayoutGroup heartLayoutGroup;

	Animator scoreAnimator;
	TextMeshProUGUI scoreText;

	public bool visibleWhenIdle = true;

	void Start() {
		gameObject.GetComponentInParent<Health>().AddHealthChangedListener(OnHealthChanged);

		var playerScore = gameObject.GetComponentInParent<PlayerScore>();
		if (playerScore)
			playerScore.AddScoreChangeListener(OnScoreChanged);

		heartLayoutGroup = transform.Find("HeartsContainer").GetComponentInChildren<HorizontalLayoutGroup>();
		var score = transform.Find("Score");
		scoreAnimator = score.GetComponent<Animator>();
		scoreText = score.GetComponentInChildren<TextMeshProUGUI>();
	}

	void LateUpdate () {
		// transform.LookAt(Camera.main.transform);
		transform.rotation = Quaternion.LookRotation(transform.position - Camera.main.transform.position);
	}

	void OnHealthChanged(int currentHealth, int previousHealth) {
		var heartsDisplayCount = heartLayoutGroup.transform.childCount;

		if (heartsDisplayCount > currentHealth) {
			int i = 0;
			for (; i < currentHealth; i++) {
				heartLayoutGroup.transform.GetChild(i).GetComponent<Animator>().SetTrigger("Flash");
			}

			for (; i < heartsDisplayCount; i++) {
				heartLayoutGroup.transform.GetChild(i).GetComponent<Animator>().SetTrigger("Death");
			}
		}

		else if (heartsDisplayCount < currentHealth) {
			int i = 0;
			for (; i < heartsDisplayCount; i++) {
				heartLayoutGroup.transform.GetChild(i).GetComponent<Animator>().SetTrigger("Flash");
			}

			for (; i < currentHealth; i++) {
				var newHeart = Instantiate(healthIconPrefab, heartLayoutGroup.transform);
				newHeart.GetComponent<Animator>().SetTrigger("Flash");
			}
		}
	}

	void OnScoreChanged(int newScore, int previousScore) {
		scoreText.text = newScore.ToString();
		scoreAnimator.SetTrigger("Flash");
	}
}
