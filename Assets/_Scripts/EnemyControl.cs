﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyControl : CharacterControl {
	GameObject[] players;

	public float sqrStalkDistance = 10f;

	NavMeshAgent agent;
	NavMeshPath path;
	float sqrPathDistance = 0.3f;
	Vector3 oldTargetPosition;
	float currentTargetUpdateTime;
	float targetUpdateTime = 1;

	public override void OnStartServer() {
		players = GameObject.FindGameObjectsWithTag("Player");

		agent = gameObject.GetComponent<NavMeshAgent>();
		agent.enabled = true;
		path = new NavMeshPath();
		agent.updatePosition = false;
		agent.updateRotation = false;

		currentTargetUpdateTime = 0;
	}

	Vector3 GetNextWaypointTarget(Vector3[] corners) {
		for (int i = 1; i < corners.Length; i++) {
			Vector3 corner = corners[i];
			Vector3 distance = corner - transform.position;

			if (distance.sqrMagnitude > sqrPathDistance)
				return distance;
		}

		return Vector3.zero;
	}

	public override void GetMovementInput() {
		TryUpdateTargetPath();

		// - - - - - - - - - - - -

		if (path.status != NavMeshPathStatus.PathInvalid) {
			Vector3 direction = GetNextWaypointTarget(path.corners);

			if (direction != Vector3.zero) {
				float angle = Vector3.SignedAngle(direction, transform.forward, Vector3.up);

				float rotationInput = 0;

				if (angle < -5.0F)
					rotationInput = 1;
				else if (angle > 5.0F)
					rotationInput = -1;

				movement.SteerMovement(1, rotationInput);
			}
		}

		movement.SteerMovement(0, 0);
	}

	public override void OnMoved() {
		agent.nextPosition = transform.position;
	}

	GameObject GetTarget() {
		float minDistance = Mathf.Infinity;
		GameObject target = null;

		foreach (var player in players) {
			Vector3 distanceVector = player.transform.position - transform.position;
			float sqrDistance = distanceVector.sqrMagnitude;

			if (sqrDistance < minDistance) {
				minDistance = sqrDistance;
				target = player;
			}
		}

		if (minDistance <= sqrStalkDistance) {
			return target;
		}

		return null;
	}

	void TryUpdateTargetPath() {
		currentTargetUpdateTime += Time.deltaTime;
		if (currentTargetUpdateTime > targetUpdateTime) {
			currentTargetUpdateTime = 0;

			var target = GetTarget();

			if (target != null)
				agent.CalculatePath(target.transform.position, path);
			else
				path.ClearCorners();
		}
	}
}
