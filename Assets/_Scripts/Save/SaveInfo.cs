﻿using System;
using System.Collections.Generic;

[Serializable]
public class SaveInfo {
	public List<ScoreInfo> scores = new List<ScoreInfo>();
}
