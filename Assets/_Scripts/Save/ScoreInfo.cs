﻿using System;

[Serializable]
public class ScoreInfo {
	public string playerName;
	public int score;

	public ScoreInfo(string _playerName, int _score) {
		playerName = _playerName;
		score = _score;
	}
}
