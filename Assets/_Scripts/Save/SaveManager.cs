﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveManager {
	public SaveInfo save;

	public delegate void OnScoresChanged(SaveInfo saveInfo);
	OnScoresChanged onScoresChanged;

	public SaveManager(OnScoresChanged callback) {
		LoadGame();
		onScoresChanged = callback;
	}

	public void AddScoreEntry(string name, int score) {
		save.scores.Add(new ScoreInfo(name, score));
		save.scores.Sort((x, y) => {return x.score > y.score ? -1 : 1;});

		SaveGame();
		onScoresChanged(save);
	}

	public void SaveGame() {
		var saveJson = JsonUtility.ToJson(save, true);
		PlayerPrefs.SetString("Save", saveJson);
	}

	public void LoadGame() {
		var saveJson = PlayerPrefs.GetString("Save", "");
		save = JsonUtility.FromJson<SaveInfo>(saveJson);
		if (save == null)
			save = new SaveInfo();
	}
}
