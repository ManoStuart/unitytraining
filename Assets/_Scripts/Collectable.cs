﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Collectable : NetworkBehaviour {
	public int pointsWorth = 1;

	public AudioSource spawnAudio;

	public delegate void OnDeath();
    OnDeath onDeath;

	public override void OnStartServer() {
		GetComponent<Collider>().enabled = true;
	}

	public override void OnStartClient() {
		spawnAudio.Play();
	}

	void OnTriggerEnter(Collider collider) {
		collider.gameObject.GetComponent<PlayerScore>().AddScore(pointsWorth);

		Destroy(gameObject);

		if (onDeath != null)
			onDeath();
	}

	public void AddDeathListener(OnDeath callback) {
		onDeath += callback;
	}
}
