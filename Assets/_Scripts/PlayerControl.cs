﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerControl : CharacterControl {
	UIController uIController;

	EndGameController endGameController;

	public AudioSource win;
	public AudioSource lose;

	public override void OnStartServer() {
		var gameController = GameObject.FindGameObjectWithTag("GameController");
		endGameController = gameController.GetComponent<EndGameController>();
	}

	public override void OnStartLocalPlayer() {
		Camera.main.transform.position = transform.position + new Vector3(0, 2.5f, -3.5f);
		gameObject.GetComponent<MSCameraController>().enabled = true;

		uIController = GameObject.FindGameObjectWithTag("UI").GetComponent<UIController>();
		uIController.mainPlayer = this;

		CmdFetchScores();
	}

    public override void GetMovementInput() {
		if (inputEnabled && Input.GetButtonDown("Jump")) {
			if (movement.TryJump()) {
				return;
			}
		}

        movement.SteerMovement(Input.GetAxis("Vertical"), Input.GetAxis("Horizontal"));
    }

	[TargetRpc]
	public void TargetRpcEndGame(NetworkConnection target, bool isWinner, int score) {
		SetInputEnabled(false);

		if (isWinner) {
			uIController.showPopup(score);
			win.Play();
		}
		else {
			lose.Play();
			uIController.showLoserPopup();
		}
	}

	[TargetRpc]
	public void TargetRpcUpdateRanking(NetworkConnection target, string scoresJson) {
		SaveInfo saveInfo = JsonUtility.FromJson<SaveInfo>(scoresJson);
		uIController.UpdateRanking(saveInfo);
	}

	[Command]
	public void CmdSaveScore(string name) {
		endGameController.SaveScore(name);
	}

	[Command]
	public void CmdFetchScores() {
		TargetRpcUpdateRanking(connectionToClient, endGameController.GetSaveJson());
	}
}
