﻿using UnityEngine;
using UnityEngine.UI;

public class HeartUI : MonoBehaviour {
	Animator animator;
	Image image;

	void Awake() {
		animator = gameObject.GetComponent<Animator>();
		image = gameObject.GetComponent<Image>();
	}

	public void OnStartAnimation() {
		image.enabled = true;
	}

	public void OnFinishDeathAnimation() {
		Destroy(animator.gameObject);
	}

	public void OnFinishFlashAnimation() {
		image.enabled = gameObject.GetComponentInParent<CharacterHud>().visibleWhenIdle;
	}
}
