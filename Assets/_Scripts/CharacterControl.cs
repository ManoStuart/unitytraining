using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(Health))]
[RequireComponent(typeof(Movement))]
public abstract class CharacterControl : NetworkBehaviour {
	public GameObject characterHudPrefab;

	protected Movement movement;
	protected bool inputEnabled;

	public AudioSource spawn;

	Vector3 startPosition;
	int startLayer;

	void Start() {
		Instantiate(characterHudPrefab, gameObject.transform);

		movement = gameObject.GetComponent<Movement>();
		inputEnabled = true;

		var health = gameObject.GetComponent<Health>();
		health.AddDeathListener(OnDeath);
		health.AddRespawnListener(OnRespawn);

		if(!hasAuthority) {
			gameObject.GetComponent<Rigidbody>().isKinematic = true;
		}

		var damager = gameObject.GetComponentInChildren<Damager>();
		if(!isServer)
			damager.gameObject.GetComponent<Collider>().enabled = false;
		else
			damager.isServer = true;

		spawn.Play();

		startPosition = transform.position;
		startLayer = gameObject.layer;
	}

    void Update () {
		if (hasAuthority) {
			if (inputEnabled)
				GetMovementInput();
			else
				movement.SteerMovement(0, 0);

			movement.updatePosition();

			OnMoved();

			movement.UpdateMovementAnimation();
		}
    }

	virtual public void GetMovementInput() {}

	virtual public void OnMoved() {}

	public void SetInputEnabled(bool isEnabled) {
		inputEnabled = isEnabled;
	}

    void OnDeath(GameObject _gameObject) {
		gameObject.layer = 12; // Dead layer
        SetInputEnabled(false);
    }

	void OnRespawn(GameObject _gameObject) {
		gameObject.layer = startLayer;
        SetInputEnabled(true);

		transform.position = startPosition;
	}
}
