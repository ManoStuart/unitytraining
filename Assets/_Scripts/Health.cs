﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(Animator))]
public class Health : NetworkBehaviour {
	static float respawnTime = 8;

	public float invulnerabilityTimeAfterHit = 1;
	float currentInvulnerabilityTimer = 0;
	bool invulnerable = false;

	public int maxHealth = 3;

	[SyncVar(hook = "onHealthChanged")]
	int currentHealth;

	public delegate void OnDeath(GameObject gameObject);
    OnDeath onDeath;

	public delegate void OnRespawn(GameObject gameObject);
    OnRespawn onRespawn;

	public delegate void OnHealthChangedCallback(int currentHealth, int previousHealth);
    OnHealthChangedCallback onHealthChangedCallback;

	Animator animator;

	public AudioSource deathAudio;

	public bool canRespawn = false;

	void Start () {
		currentHealth = maxHealth;
		animator = gameObject.GetComponent<Animator>();

		if (onHealthChangedCallback != null)
			onHealthChangedCallback(currentHealth, 0);
	}

	void Update() {
		currentInvulnerabilityTimer -= Time.deltaTime;

		if (currentInvulnerabilityTimer <= 0) {
			enabled = false;
		}
	}

	public bool isInvulnerable() {
		return currentInvulnerabilityTimer > 0 || invulnerable;
	}

	public bool TryTakeDamage(int damage) {
		if (!isServer || isInvulnerable())
			return false;

		currentHealth -= damage;

		if (currentHealth <= 0) {
			if (hasAuthority)
				Death();
			else
				TargetRpcDeath(connectionToClient);

			if (canRespawn)
				Invoke("Respawn", respawnTime);
			else
				Invoke("Purge", 4);
		}

		return true;
	}

	public void AddDeathListener(OnDeath callback) {
		onDeath += callback;
	}

	public void AddRespawnListener(OnRespawn callback) {
		onRespawn += callback;
	}

	public void AddHealthChangedListener(OnHealthChangedCallback callback) {
		onHealthChangedCallback += callback;
	}

	public void onHealthChanged(int newHealth) {
		enabled = true;
		currentInvulnerabilityTimer = invulnerabilityTimeAfterHit;

		if (onHealthChangedCallback != null)
			onHealthChangedCallback(newHealth, currentHealth);
	}

	[TargetRpc]
	void TargetRpcDeath(NetworkConnection target) {
		Death();
	}

	void Death() {
		invulnerable = true;
		animator.SetBool("Dead", true);

		if (onDeath != null)
			onDeath(gameObject);

		deathAudio.Play();
	}

	void Purge() {
		Destroy(gameObject);
	}

	void Respawn() {
		TargetRpcRespawn(connectionToClient);
	}

	[TargetRpc]
	void TargetRpcRespawn(NetworkConnection target) {
		invulnerable = false;
		animator.SetBool("Dead", false);
		currentHealth = maxHealth;

		if (onRespawn != null)
			onRespawn(gameObject);

		deathAudio.Play();
	}
}
