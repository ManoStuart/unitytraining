using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(Rigidbody))]
public class Movement : NetworkBehaviour{
    public enum ForceType {noForce, jump, doubleJump, knockback, attack};

    public struct ForceInfo {
        public ForceType forceType;
        public float force;
        public string[] animationTriggers;
        public Vector3 defaultDirection;
        public bool resetVelocity;
        public AudioSource audioSource;

        public ForceInfo(ForceType _forceType, float _force, string[] _animationTriggers, Vector3 _defaultDirection, bool _resetVelocity, AudioSource _audioSource) {
            forceType = _forceType;
            force = _force;
            animationTriggers = _animationTriggers;
            defaultDirection = _defaultDirection;
            resetVelocity = _resetVelocity;
            audioSource = _audioSource;
        }
    }

    Animator animator;
    Rigidbody myRigidbody;

    float currentRotationSpeed;
    float currentMovementSpeed;
    int groundedSemaphore;

    const float interpolationTime = 5;

    public float maxMoveSpeed = 2;
    public float maxTurnSpeed = 200;
    public float jumpForce = 6;
    public float doubleJumpForce = 6;
    public float knockbackForce = 6;

    public Dictionary<ForceType, ForceInfo> forces;

	public AudioSource jump;
	public AudioSource double_jump;
	public AudioSource hit;

    public void Start() {
        animator = gameObject.GetComponent<Animator>();
        myRigidbody = gameObject.GetComponent<Rigidbody>();

        currentRotationSpeed = 0;
        currentMovementSpeed = 0;
        groundedSemaphore = 0;

        forces = new Dictionary<ForceType, ForceInfo> {
            {ForceType.jump, new ForceInfo(ForceType.jump, jumpForce, new string[] {"Jump"}, Vector3.up, false, jump)},
            {ForceType.doubleJump, new ForceInfo(ForceType.doubleJump, doubleJumpForce, new string[] {"Jump", "Attack"}, Vector3.up, true, double_jump)},
            {ForceType.knockback, new ForceInfo(ForceType.knockback, knockbackForce, new string[] {"Jump"}, Vector3.zero, false, hit)},
            {ForceType.attack, new ForceInfo(ForceType.attack, 0, new string[] {"Attack"}, Vector3.zero, false, null)},
        };
    }

    public void SteerMovement(float verticalInput, float horizontalInput) {
        currentMovementSpeed = Mathf.Lerp(currentMovementSpeed, verticalInput, Time.deltaTime * interpolationTime);
        currentRotationSpeed = Mathf.Lerp(currentRotationSpeed, horizontalInput, Time.deltaTime * interpolationTime);
    }

    public void updatePosition() {
        transform.Rotate(0, currentRotationSpeed * maxTurnSpeed * Time.deltaTime, 0);
        var velocity = transform.forward * currentMovementSpeed * maxMoveSpeed;

        myRigidbody.MovePosition(transform.position + velocity * Time.deltaTime);
    }

    public void UpdateMovementAnimation() {
        var velocity = myRigidbody.velocity;
        velocity.y = 0;

        animator.SetFloat("Speed", currentMovementSpeed);
        animator.SetFloat("SpeedY", myRigidbody.velocity.y);
    }

    public bool TryJump() {
        if (IsGrounded()) {
            ApplyForce(ForceType.jump, Vector3.up);
            CmdPlaySound(ForceType.jump);

            return true;
        }

        return false;
    }

    public void ServerApplyForce(ForceType forceType, Vector3 direction) {
        if (connectionToClient == null)
            ApplyForce(forceType, -direction);
        else
            TargetRpcApplyForce(connectionToClient, forceType, -direction);

        RpcPlaySound(forceType);
    }

    [TargetRpc]
    void TargetRpcApplyForce(NetworkConnection target, ForceType forceType, Vector3 direction) {
        ApplyForce(forceType, direction);
    }

    void ApplyForce(ForceType forceType, Vector3 direction) {
		if (!hasAuthority)
            return;

        if (!forces.ContainsKey(forceType))
            return;

        ForceInfo forceInfo = forces[forceType];

        if (forceInfo.resetVelocity)
            myRigidbody.velocity = Vector3.zero;

        direction = forceInfo.defaultDirection != Vector3.zero ? forceInfo.defaultDirection : direction;

        myRigidbody.AddForce(direction * forceInfo.force, ForceMode.Impulse);

        foreach (var trigger in forceInfo.animationTriggers) {
            animator.SetTrigger(trigger);
        }
    }

    public void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.layer == 8) { // Ground
            groundedSemaphore += 1;

            animator.SetBool("Grounded", IsGrounded());
        }
    }

    public void OnCollisionExit(Collision collision) {
        if (collision.gameObject.layer == 8) { // Ground
            groundedSemaphore -= 1;

            if (groundedSemaphore < 0)
                Debug.LogError(string.Format("{0}: Wrong grounded collision count", gameObject.name));

            animator.SetBool("Grounded", IsGrounded());
        }
    }

    bool IsGrounded() {
        return groundedSemaphore > 0;
    }

	[Command(channel=1)]
	void CmdPlaySound(ForceType forceType) {
        RpcPlaySound(forceType);
	}

	[ClientRpc(channel=1)]
	void RpcPlaySound(ForceType forceType) {
        if (!forces.ContainsKey(forceType))
            return;

        ForceInfo forceInfo = forces[forceType];

        if (forceInfo.audioSource != null)
            forceInfo.audioSource.Play();
	}

}
