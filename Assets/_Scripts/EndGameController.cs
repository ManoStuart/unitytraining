﻿using UnityEngine;
using UnityEngine.Networking;

public class EndGameController : NetworkBehaviour {
	EnemySpawner spawner;

	SaveManager saveManager;

	float startTime;
	float endTime;

	void Awake () {
		spawner = GetComponent<EnemySpawner>();
		saveManager = new SaveManager(OnScoresChanged);

		startTime = Time.realtimeSinceStartup;
	}

	int GetScore() {
		return 2000 - (int)(endTime - startTime);
	}

	public void EndGame(GameObject winner) {
		var players = GameObject.FindGameObjectsWithTag("Player");
		endTime = Time.realtimeSinceStartup;

		foreach (var player in players) {
			var control = player.GetComponent<PlayerControl>();
			control.TargetRpcEndGame(control.connectionToClient, player == winner, GetScore());
		}

		// - - - - - - - - - - - - - - - - - - -

		spawner.enabled = false;

		var enemies = GameObject.FindGameObjectsWithTag("Enemy");
		foreach (var enemy in enemies) {
			enemy.GetComponent<EnemyControl>().SetInputEnabled(false);
		}
	}

	public void SaveScore(string name) {
		saveManager.AddScoreEntry(name, GetScore());
	}

	public void OnScoresChanged(SaveInfo save) {
		var players = GameObject.FindGameObjectsWithTag("Player");

		foreach (var player in players) {
			var control = player.GetComponent<PlayerControl>();
			control.TargetRpcUpdateRanking(control.connectionToClient, JsonUtility.ToJson(save));
		}
	}

	public string GetSaveJson() {
		return JsonUtility.ToJson(saveManager.save);
	}
}
