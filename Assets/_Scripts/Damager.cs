﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class Damager : MonoBehaviour {
	Movement movement;

    [TagSelector] public string[] targetTags;

    public Movement.ForceType selfForceType;
	public Movement.ForceType targetForceType;

	public int damage = 1;

	bool inactive = false;

	public bool isServer = false;

    void Start() {
		movement = gameObject.GetComponentInParent<Movement>();

		Health health = gameObject.GetComponentInParent<Health>();

		if (health) {
			health.AddDeathListener(OnDeath);
			health.AddRespawnListener(OnRespawn);
		}
	}

	public virtual void OnTriggerEnter(Collider collider) {
		if (!isServer)
			return;

		if (!inactive && IsTarget(collider.gameObject) && gameObject != collider.gameObject) {
			if (collider.gameObject.GetComponent<Health>().TryTakeDamage(damage)) {
				Vector3 direction = collider.transform.position - transform.position;
				direction.y = 0;
				direction = direction.normalized;

				movement.ServerApplyForce(selfForceType, direction);

				Movement targetMovement = collider.gameObject.GetComponent<Movement>();
				targetMovement.ServerApplyForce(targetForceType, -direction);
			}
		}
	}

	bool IsTarget(GameObject gameObject) {
		foreach (var tag in targetTags)
		{
			if (gameObject.CompareTag(tag))
				return true;
		}

		return false;
	}

    void OnDeath(GameObject _gameObject) {
        gameObject.GetComponent<Collider>().enabled = false;
		inactive = true;
    }

    void OnRespawn(GameObject _gameObject) {
        gameObject.GetComponent<Collider>().enabled = true;
		inactive = false;
    }
}
