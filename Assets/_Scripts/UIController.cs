﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using TMPro;

public class UIController : MonoBehaviour {
	public TextMeshProUGUI scoreText;
	public TextMeshProUGUI scoreInput;
	public GameObject inputUI;
	public GameObject loserUI;

	public PlayerControl mainPlayer;

	public GameObject rankCellPrefab;
	public Transform rankingHolder;

	public void showPopup(int score) {
		scoreText.text = string.Format("Score: {0}", score);
		scoreInput.text = "";
		inputUI.SetActive(true);
	}

	public void showLoserPopup() {
		loserUI.SetActive(true);
	}

	public void OnSubmitScore() {
		mainPlayer.CmdSaveScore(scoreInput.text);
		inputUI.SetActive(false);
	}

	public void UpdateRanking(SaveInfo saveInfo) {
 		foreach(Transform child in rankingHolder)
		 	Destroy(child.gameObject);

		var scores = saveInfo.scores.GetRange(0, Mathf.Min(3, saveInfo.scores.Count));

		foreach (var scoreInfo in scores) {
			var rankCell = Instantiate(rankCellPrefab, rankingHolder);
			var text = rankCell.GetComponentInChildren<TextMeshProUGUI>();
			text.text = string.Format("{0}: {1}", scoreInfo.playerName, scoreInfo.score);
		}
	}
}
