﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerScore : NetworkBehaviour {
	static int TargetScore = 5;

	[SyncVar(hook = "OnScoreChanged")]
	int currentScore = 0;

	public delegate void OnScoreChangedCallback(int newScore, int previousScore);
    OnScoreChangedCallback onScoreChangedCallback;

	public AudioSource collectAudio;

	public void AddScoreChangeListener(OnScoreChangedCallback callback) {
		onScoreChangedCallback += callback;
	}

	public void AddScore(int plusScore) {
		if (!isServer)
			return;

		currentScore += plusScore;
		RpcPlayCollectSound();

		if (currentScore >= TargetScore) {
			var gameController = GameObject.FindGameObjectWithTag("GameController");
			gameController.GetComponent<EndGameController>().EndGame(gameObject);
		}
	}

	public void OnScoreChanged(int newScore) {
		if (onScoreChangedCallback != null)
			onScoreChangedCallback(newScore, currentScore);
	}

	[ClientRpc(channel=1)]
	void RpcPlayCollectSound() {
		collectAudio.Play();
	}
}
