﻿using UnityEngine;
using UnityEngine.Networking;

public class EnemySpawner : NetworkBehaviour {
	public GameObject enemyPrefab;
	public GameObject collectablePrefab;

	public int numberOfEnemies;
	public float enemySpawnTime = 3;
	float currentEnemySpawnTime;

	public int numberOfCollectables;
	public float collectableSpawnTime = 3;
	float currentCollectableSpawnTime;

	Transform topRightBoundary;
	Transform bottomLeftBoundary;

	public int maxEnemyCount = 50;
	int currentEnemyCount = 20;

	public int maxCollectableCount;
	int currentCollectableCount;

	public override void OnStartServer () {
		currentEnemySpawnTime = 1;
		currentCollectableSpawnTime = 1;

		topRightBoundary = transform.Find("TopRightBoundary");
		bottomLeftBoundary = transform.Find("BottomLeftBoundary");
	}

	void Update() {
		currentEnemySpawnTime -= Time.deltaTime;
		if (currentEnemySpawnTime <= 0 && currentEnemyCount < maxEnemyCount) {
			for (int i = 0; i < numberOfEnemies; i++) {
				SpawnEnemy();
			}

			currentEnemySpawnTime = enemySpawnTime;
		}


		currentCollectableSpawnTime -= Time.deltaTime;
		if (currentCollectableSpawnTime <= 0 && currentCollectableCount < maxCollectableCount) {
			for (int i = 0; i < numberOfCollectables; i++) {
				SpawnCollectable();
			}

			currentCollectableSpawnTime = collectableSpawnTime;
		}
	}

	Vector3 GetSpawnPosition() {
		var raycastOrigin = new Vector3(
			Random.Range(bottomLeftBoundary.position.x, topRightBoundary.position.x),
			transform.position.y,
			Random.Range(bottomLeftBoundary.position.z, topRightBoundary.position.z)
		);

		RaycastHit hit;
        if (Physics.Raycast(raycastOrigin, Vector3.down, out hit, Mathf.Infinity, 1 << 8))
			return hit.point;
        else
			Debug.LogError("Couldn't find spawn position");

		return Vector3.zero;
	}

	void SpawnEnemy() {
		var spawnPosition = GetSpawnPosition();

		var SpawnRotation = Quaternion.Euler(
			0.0f,
			Random.Range(0, 180),
			0.0f
		);

		var enemy = (GameObject)Instantiate(enemyPrefab, spawnPosition, SpawnRotation);
		NetworkServer.Spawn(enemy);

		enemy.GetComponent<Health>().AddDeathListener(OnEnemyDeath);
		currentEnemyCount++;
	}

	void SpawnCollectable() {
		var spawnPosition = GetSpawnPosition();

		var SpawnRotation = Quaternion.Euler(
			-90f,
			Random.Range(0, 180),
			0.0f
		);

		var enemy = (GameObject)Instantiate(collectablePrefab, spawnPosition, SpawnRotation);
		NetworkServer.Spawn(enemy);

		enemy.GetComponent<Collectable>().AddDeathListener(OnCollectableDeath);
		currentCollectableCount++;
	}

	void OnEnemyDeath(GameObject gameObject) {
		currentEnemyCount--;
	}

	void OnCollectableDeath() {
		currentCollectableCount--;
	}
}
