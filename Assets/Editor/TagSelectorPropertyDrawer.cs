using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

[CustomPropertyDrawer(typeof(TagSelectorAttribute))]
public class TagSelectorPropertyDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);
        
        if (property.propertyType == SerializedPropertyType.String) {
            string layerName = property.stringValue;
            EditorGUI.BeginChangeCheck();

            if (string.IsNullOrEmpty(layerName)) {
                layerName = "Untagged";
                property.stringValue = layerName;
            }
            layerName = EditorGUI.TagField(position, label, layerName);

            if (EditorGUI.EndChangeCheck()) {
                property.stringValue = layerName;
            }
        }
        else
            EditorGUI.LabelField(position, label, typeof(TagSelectorAttribute).Name + " only allow to use with { String }.");

        EditorGUI.EndProperty();
    }
}